var express = require("express");
var app = new express();
var http = require("http").Server(app);
var io = require("socket.io")(http);

var Log  = require("log"),
	log = new Log("debug");

var port = process.env.PORT || 4000;

app.use(express.static(__dirname + "/public"));

app.get('/', function(req, res){
	res.redirect('/views/index.html');
});
//------------------------------------------------------------
var s_horarios = new Object();
var u_activos = new Object();

io.on('connection', function(socket){

	socket.on('contectarSala', function(sala, sem, u_id, u_name){
		if( typeof s_horarios[sala] == 'undefined' ){
			s_horarios[sala] = {};
			s_horarios[sala][sem] = {};
		} else {
			if( typeof s_horarios[sala][sem] == 'undefined' ){
				s_horarios[sala][sem] = {};
			}
		}		

		if( typeof u_activos[u_id] == 'undefined'){
			u_activos[u_id] = u_name;
			socket.join(sala, function(){
			   io.sockets.to(sala).emit("refresh_u_activos", u_activos);
		  	});
		}

		socket.emit("refresh_horario", s_horarios[sala][sem]);
	});

	socket.on('reservar', function(sala, sem, dia, ini, fin, u_id){

		if( typeof s_horarios[sala][sem][dia] !== 'undefined' ){
			s_horarios[sala][sem][dia].push({
				"u_id" : u_id,
				"ini" : ini,
				"fin" : fin
			});
		} else {

			s_horarios[sala][sem][dia] = [			
				{
					"u_id" : u_id,
					"ini" : ini,
					"fin" : fin
				}
			]
		}

	    io.sockets.to(sala).emit("refresh_horario", s_horarios[sala][sem]);
	});

	socket.on('refresh_horario', function(sala, sem){
		socket.emit("refresh_horario", s_horarios[sala][sem]);
	});

});

http.listen(port, function(){
	log.info("servidor escuchando a traves del puerto %s", port);
});