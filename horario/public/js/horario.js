var socket = io.connect('http://localhost:4000');

var horario = {};
var usuarios = {};

$(document).ready(function(){

	$("#btn_conectar").click(function(){
		socket.emit("contectarSala", $("#sala").val(), $("#sem").val(), $("#u_id").val(), $("#u_name").val());
		presentar_horario();
	});

	$("#btn_reservar").click(function(){
		socket.emit("reservar", $("#sala").val(), $("#sem").val(), $("#dia").val(), $("#ini").val(), $("#fin").val(), $("#u_id").val());
	});
});


$(function(){
	socket.on("mensaje", function(msg){
    	alert(msg.contenido);
    });

    socket.on("refresh_horario", function(sem){
    	horario = sem;
    	console.log(sem);
    });

    socket.on("refresh_u_activos", function(usrs){
    	usuarios = usrs;
    	console.log(usrs);
    });    
});

function presentar_horario(){
	array_sem = $("#sem").val().split("-");
	//alert(array_sem);
	var dt = new Date(array_sem[0]);

	for (var i = 1; i <= 7; i++) {		
		$("#d"+i).html(dt);
		dt.setDate(dt.getDate()+1);
	}
}